﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public Text scoreText;

    public float spawnDelay;
    public float startDelay;

    private float timeExpired;
    private float timeItemToBeTargeted = 24f;

    public Rigidbody goat;
    public Rigidbody pig;


    public List<GameObject> gameObjectsToChooseFrom = new List<GameObject>();

    private int score;

	// Use this for initialization
	void Start () {
        score = 3;
        UpdateScore();
        Instantiate(goat, new Vector3(0, 0, 0), transform.localRotation);
        Instantiate(pig, new Vector3(-25, 0, 0), transform.localRotation);
        StartCoroutine(spawnGameObjects());
	}

    private void Update()
    {
        timeExpired += Time.deltaTime;
        score = (int)timeExpired;
        UpdateScore();
    }
    IEnumerator spawnGameObjects()
    {
        yield return new WaitForSeconds(spawnDelay);
    }
	
    public void selectNewTarget(GameObject curTarget)
    {
        
    }

    public void addScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore ()
    {
        scoreText.text = "Score: " + score;
    }

}
