﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu_UI : MonoBehaviour {

	public Button startGameBtn;
	// public Button exitGameBtn;

	void Start() {
		Button sgBtn = startGameBtn.GetComponent<Button>();
		// Button egBtn = exitGameBtn.GetComponent<Button>();

		sgBtn.onClick.AddListener(delegate { LoadScene("Game_MainPlay"); });
		// egBtn.onClick.AddListener(delegate { ExitGame(); });
	}

	public void LoadScene(string sceneName) {
		SceneManager.LoadScene(sceneName);
	}

    public void ExitGame() {
		Application.Quit();
	}

}
